var form = document.getElementById("formConditional")
var jawaban = document.getElementById("jawaban")

form.addEventListener("submit", function(event) {
    event.preventDefault()
    var name = document.getElementById("name").value
    var role = document.getElementById("role").value  
    console.log(name) // nama yang disubmit akan tampil di console
    jawaban.innerHTML = name + " " + role
})