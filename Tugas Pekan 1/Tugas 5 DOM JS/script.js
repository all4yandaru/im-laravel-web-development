var items = [
    ['001', 'Keyboard Logitek', 60000, 'Keyboard yang mantap untuk kantoran', 'logitek.jpg'], 
    ['002', 'Keyboard MSI', 300000, 'Keyboard gaming MSI mekanik', 'msi.jpg'],
    ['003', 'Mouse Genius', 50000, 'Mouse Genius biar lebih pinter', 'genius.jpeg'],
    ['004', 'Mouse Jerry', 30000, 'Mouse yang disukai kucing', 'jerry.jpg']
]

var listBarang = document.getElementById("listBarang")
var searchButton = document.getElementById("formItem")
var keyword = document.getElementById("keyword")
var cart = document.getElementById("cart")

var cartTotal = 0


const compareString = (word1, word2) => {
	word1 = word1.toLowerCase();

	word2 = word2.value.toLowerCase();

	return word1.includes(word2)
}

const showData = (itemsLocal, keyword = "") => {
	var data = ""

	for (var i = 0; i < itemsLocal.length; i++) {
		var number = itemsLocal[i][2].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")

		if (keyword == "") {
			data += `
				<div class="col-4 my-2" >
	              <div class="card h-100">
	                <img src="./img/${itemsLocal[i][4]}" class="card-img-top mx-auto d-block" alt="${itemsLocal[i][4]}">
	                <div class="card-body">
	                    <h5 class="card-title" id="itemName">${itemsLocal[i][1]}</h5>
	                    <p class="card-text" id="itemDesc">${itemsLocal[i][3]}</p>
	                    <p class="card-text">${itemsLocal[i][2]}</p>
	                    <a href="#" class="btn btn-primary" id="addCart">Tambahkan ke keranjang</a>
	                </div>
	              </div>
	            </div>`
		}
		else if (compareString(itemsLocal[i][1], keyword)) {
			data += `
				<div class="col-4 my-2" >
	              <div class="card h-100">
	                <img src="./img/${itemsLocal[i][4]}" class="card-img-top mx-auto d-block" alt="${itemsLocal[i][4]}">
	                <div class="card-body">
	                    <h5 class="card-title" id="itemName">${itemsLocal[i][1]}</h5>
	                    <p class="card-text" id="itemDesc">${itemsLocal[i][3]}</p>
	                    <p class="card-text">${itemsLocal[i][2]}</p>
	                    <a href="#" class="btn btn-primary" id="addCart">Tambahkan ke keranjang</a>
	                </div>
	              </div>
	            </div>`
		}
	}
	listBarang.innerHTML = data

	var addCart = document.querySelectorAll("#addCart");

	addCart.forEach(tombol => {
    tombol.addEventListener("click", () => {
      cartTotal++;
      cart.innerHTML = `<i class="fas fa-shopping-cart"></i>(${cartTotal})`;
    })
  })
}

showData(items)

searchButton.addEventListener("submit", function(event) {
        event.preventDefault()

        var keywordValue = keyword.value
        console.log(keywordValue)

        showData(items, keyword)
    })

cart.addEventListener("click", function(event) {
        event.preventDefault()

        cart.innerHTML = `<i class="fas fa-shopping-cart"></i>(0)`
    })