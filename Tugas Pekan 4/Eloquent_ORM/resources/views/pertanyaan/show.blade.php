@extends('adminLTE.master')

@section('content')
<div class="mt-3 ml-3">
    <h4>{{ $data->judul}}</h4>
    <p>{{ $data->isi }}</p>
    <p>dibuat : {{ $data->tanggal_dibuat }}</p>
    <p>diperbaharui : {{ $data->tanggal_diperbaharui }}</p>
</div>
@endsection