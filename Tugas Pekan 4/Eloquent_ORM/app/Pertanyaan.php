<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pertanyaan extends Model
{
    protected $table = "pertanyaan";

    protected $fillable = ["judul", "isi", "tanggal_dibuat", "tanggal_diperbaharui"];

    /*protected $guarded = []; // semua kolom bisa diisi
    protected $guarded = ["body"] // body ga boleh dimasukin ke kolom*/
}
