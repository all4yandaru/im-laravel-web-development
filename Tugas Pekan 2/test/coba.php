<?php  
	function perolehan_medali(array $data){
		$data_length = count($data);
		$array_assoc = array();

		$emas = 0;
		$perak = 0;
		$perunggu = 0;
		$country = $data[0][0];
		$jumlah = 0;

		for ($i=0; $i < $data_length; $i++) {

			if (getExsist($data, $country, $i) && $i > 0) {
				break;
			}
			else if (in_array($country, $data[$i])) {
				$emas = getCounter($data, $country, 'emas');
				$perak = getCounter($data, $country, 'perak');
				$perunggu = getCounter($data, $country, 'perunggu');

				$array_assoc[$jumlah]['negara'] = $country;
				$array_assoc[$jumlah]['emas'] = $emas;
				$array_assoc[$jumlah]['perak'] = $perak;
				$array_assoc[$jumlah]['perunggu'] = $perunggu;
				$jumlah++;
			}

			if ($i < $data_length - 1) {
				$country = $data[$i+1][0];
			}
		}
		return $array_assoc;
	}

	function getCounter(array $data, $country, $medali){
		$data_length = count($data);
		$counter = 0;

		for ($i=0; $i < $data_length; $i++) { 
			if ($data[$i][0] == $country && $data[$i][1] == $medali) {
				$counter++;
			}
		}
		return $counter;
	}

	function getExsist($data, $country, $index){
		$exist = false;
		for ($i=0; $i < $index; $i++) { 
			if ($data[$i][0] == $country) {
				$exist = true;
			}
		}
		return $exist;
	}

	print_r(perolehan_medali(
		array(
			array('Indonesia','emas'),
			array('India','perak'),
			array('Korea Selatan','emas'),
			array('India','perak'),
			array('India','emas'),
			array('Indonesia','perak'),
			array('Indonesia','emas')
		)
	));
?>