<?php
function tukar_besar_kecil($string){
	$length = strlen($string);
	$ord;

	for ($i=0; $i < $length; $i++) {
		$ord = ord($string[$i]);

		if ($ord >= 97 && $ord <= 122) {
			$ord -= 32;
		}
		else if ($ord >= 65 && $ord <= 90) {
			$ord += 32;
		}
		$string[$i] = chr($ord);
	}
	return $string . "<br>";
}

// TEST CASES
echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

?>