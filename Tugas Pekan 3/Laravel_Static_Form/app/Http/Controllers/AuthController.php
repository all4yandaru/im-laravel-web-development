<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    function register(){
    	return view('register');
    }

    function welcome(Request $request){
    	$first_name = $request['first_name'];
    	$last_name = $request['last_name'];

    	return view('view', 
    		[
    			'first_name' => $first_name,
    			'last_name' => $last_name
    	]);
    }
}
