@extends('adminlte.master')

@section('content')
<div class="my-3 mx-3">
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Edit Pertanyaan {{$data->id}}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        
        <form role="form" action="/pertanyaan/{{$data->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="card-body">
            <div class="form-group">
                <label for="title">Judul Baru</label>
                <input type="text" name="judul" class="form-control" id="judul" placeholder="Masukkan Judul Baru" value="{{ old('judul',$data->judul) }}">
                @error('judul')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">Isi Pertanyaan Baru</label>
                <input type="text" class="form-control" id="isi" name="isi" placeholder="Body" value="{{ old('isi',$data->isi) }}">
                @error('isi')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>

            
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Edit</button>
        </div>
        </form>
    </div>
</div>
@endsection