@extends('adminLTE.master')

@section('content')
<div class="my-3 mx-3">
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Buat Pertanyaan Baru</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        
        <form role="form" action="/pertanyaan" method="POST">
        @csrf
        <div class="card-body">
            <div class="form-group">
                <label for="title">Judul</label>
                <input type="text" name="judul" class="form-control" id="judul" placeholder="Masukkan Judul" value="{{ old('judul','') }}">
                @error('judul')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">Isi Pertanyaan</label>
                <input type="text" class="form-control" id="isi" name="isi" placeholder="Isi Pertanyaan" value="{{ old('isi','') }}">
                @error('isi')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>

            
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Buat Pertanyaan</button>
        </div>
        </form>
    </div>
</div>
@endsection