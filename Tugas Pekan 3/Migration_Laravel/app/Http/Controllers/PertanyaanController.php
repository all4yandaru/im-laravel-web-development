<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function index(){
    	$data = DB::table('pertanyaan')->get();
    	return view('pertanyaan.index', compact('data'));
    }

    public function create(){
    	return view('pertanyaan.create');
    }

    public function store(Request $request){
    	$request->validate([
    		"judul" => "required|unique:pertanyaan",
    		"isi" => "required"
    	]);

    	$query = DB::table('pertanyaan')->insert([
    		"judul" => $request["judul"],
    		"isi" => $request["isi"],
    		"tanggal_dibuat" => date("Y/m/d"),
    		"tanggal_diperbaharui" => date("Y/m/d")
    	]);

    	return redirect('/pertanyaan')->with('success', "Pertanyaan berhasil ditambahkan!");
    }

    public function show($id){
    	$data = $data = DB::table('pertanyaan')->where('id', $id)->first();

    	return view('pertanyaan.show', compact('data'));
    }

    public function edit($id){
    	$data = $data = DB::table('pertanyaan')->where('id', $id)->first();

    	return view('pertanyaan.edit', compact('data'));
    }

    public function update($id, Request $request){
    	$request->validate([
    		"judul" => "required|unique:pertanyaan",
    		"isi" => "required"
    	]);

    	$query = DB::table('pertanyaan')->where('id', $id)->update([
    		"judul" => $request["judul"],
    		"isi" => $request["isi"],
    		"tanggal_diperbaharui" => date("Y/m/d")
    	]);

    	return redirect('/pertanyaan')->with('success', "Pertanyaan berhasil diedit!");
    }

    public function destroy($id){
    	$query = DB::table('pertanyaan')->where('id', $id)->delete();
        return redirect('/pertanyaan')->with('success', 'Pertanyaan Berhasil didelete!');
    }
}
